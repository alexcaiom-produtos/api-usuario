/**
 * 
 */
package br.com.massuda.alexander.usuario.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.massuda.alexander.persistencia.jdbc.utils.MonitoradorDeConexao;

/**
 * @author Alex
 *
 */
@Controller
public class JDBCMonitorController {
	
	@RequestMapping("/exit")
	public void exit () {
		MonitoradorDeConexao.getInstancia(0).encerrar();
//		RestTemplate rt = new RestTemplate();
//		rt.
	}

}
