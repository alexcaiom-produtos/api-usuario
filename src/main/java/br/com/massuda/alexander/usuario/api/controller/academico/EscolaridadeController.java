package br.com.massuda.alexander.usuario.api.controller.academico;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.api.controller.Controlador;
import br.com.massuda.alexander.usuario.api.mapper.Mapper;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.Escolaridade;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.EscolaridadeDto;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.GrauDeEscolaridade;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/academico/escolaridade")
public class EscolaridadeController extends Controlador<Escolaridade> {

	public EscolaridadeController() {
		super(Escolaridade.class);
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody EscolaridadeDto dto) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		Escolaridade o = Mapper.to(dto);
		getDao().incluir(o);
	}
	
	
	@PutMapping
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public void alterar(@RequestBody EscolaridadeDto dto) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		Escolaridade o = Mapper.to(dto);
		getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public EscolaridadeDto pesquisar (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return Mapper.from(getFinder().pesquisar(id));
	}

	@ResponseBody
	@GetMapping
	public List<EscolaridadeDto> listar () throws Erro, NoSuchFieldException, SecurityException {
		return Mapper.from(getFinder().listar());
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, ErroUsuario, NoSuchFieldException, SecurityException {
		if (ObjectUtils.isNotEmpty(id)) {
			Escolaridade o = finder.pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}

	
	@ResponseBody
	@GetMapping("/nivel")
	public List<GrauDeEscolaridade> niveis () throws Erro, NoSuchFieldException, SecurityException {
		return Arrays.asList(GrauDeEscolaridade.values());
	}
}
