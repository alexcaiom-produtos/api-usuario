package br.com.massuda.alexander.usuario.api.controller.profissional;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Segmento;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/segmento")
public class SegmentoController extends Controlador<Empresa> {
	
	@Autowired
	private DAO<Segmento> dao;
	@Autowired
	private Finder<Segmento> finder;

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody Segmento segmento) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		getDao().incluir(segmento);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody Segmento segmento) throws Erro, SysErr, SQLException {
		getDao().editar(segmento);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Segmento pesquisar (@PathVariable Long id) {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Segmento> listar () throws Erro {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Segmento segmento = getFinder().pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(segmento);
		}
	}

	private DAOGenericoJDBCImpl<Segmento> getDao() {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<>(Segmento.class);
		}
		return dao;
	}
	
	private FinderJDBC<Segmento> getFinder() {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<>(Segmento.class);
		}
		return finder;
	}
}
