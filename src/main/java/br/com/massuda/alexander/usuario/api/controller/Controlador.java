package br.com.massuda.alexander.usuario.api.controller;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;

import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.waiso.framework.abstratas.Classe;

@EnableAsync(proxyTargetClass=true)
public abstract class Controlador<T> extends Classe {
	

	@Autowired
	protected HttpSession httpSession;
	@Value("${db.type}")
	private static String dbType;
	
	public Controlador(Class<T> tipo) {
		this.tipo = tipo;
	}
	
	protected DAO<T> dao;
	protected Finder<T> finder;
	private Class<T> tipo;
	

	protected DAO<T> getDao() throws NoSuchFieldException, SecurityException {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<T>(tipo, dbType);
		}
		return dao;
	}
	
	protected FinderJDBC<T> getFinder() throws NoSuchFieldException, SecurityException {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<T>(tipo, dbType);
		}
		return finder;
	}	
}
