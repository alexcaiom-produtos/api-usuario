/**
 * 
 */
package br.com.massuda.alexander.usuario.api.controller.academico;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.api.controller.Controlador;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.finder.impl.usuario.FinderEnderecoImpl;
import br.com.massuda.alexander.usuario.dao.finder.usuario.academico.impl.FinderInstituicaoDeEnsinoImpl;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.dao.usuario.academico.impl.InstituicaoDeEnsinoDAOImpl;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.CoordenadasGeograficas;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.InstituicaoDeEnsino;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;
import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */

@CrossOrigin
@Controller
@RequestMapping("/academico/instituicao-ensino")
public class InstituicaoDeEnsinoController extends Controlador<InstituicaoDeEnsino> {
	
	Finder<Endereco> finderEndereco = new Finder(Endereco.class);
	Finder<CoordenadasGeograficas> finderCoordenadasGeograficas = new Finder(CoordenadasGeograficas.class);

	public InstituicaoDeEnsinoController() {
		super(InstituicaoDeEnsino.class);
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody InstituicaoDeEnsino o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		processarEndereco(o);
		getDao().incluir(o);
	}
	
	
	@PutMapping
	@ResponseBody
	public void alterar(@RequestBody InstituicaoDeEnsino o) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		processarEndereco(o);
		getDao().editar(o);
	}
	
	private void processarEndereco(InstituicaoDeEnsino o) {
		Endereco endereco = o.getEndereco();
		if (Objects.nonNull(endereco)) {
			double latitude = endereco.getCoordenadasGeograficas().getLatitude();
			double longitude = endereco.getCoordenadasGeograficas().getLongitude();
			List<Filtro> filtros = new ArrayList<>();
			filtros.add(new Filtro(double.class, "latitude", TipoFiltro.IGUAL_A, latitude, true));
			filtros.add(new Filtro(double.class, "longitude", TipoFiltro.IGUAL_A, longitude, true));
			List<CoordenadasGeograficas> coordenadas = finderCoordenadasGeograficas.pesquisar(filtros);
			filtros.clear();
			coordenadas.forEach(c -> {
				filtros.add(new Filtro(long.class, "id", TipoFiltro.EM, c.getId(), true));
			});
			filtros.add(new Filtro(Integer.class, "numero", TipoFiltro.IGUAL_A, endereco.getNumero(), true));
			filtros.add(new Filtro(String.class, "complemento", TipoFiltro.IGUAL_A, endereco.getComplemento(), true));
			List<Endereco> enderecos = finderEndereco.pesquisar(filtros);
			if (!CollectionUtils.isEmpty(enderecos)) {
				o.setEndereco(enderecos.get(0));
			}
		}
	}

	@ResponseBody
	@GetMapping("/id/{id}")
	public InstituicaoDeEnsino pesquisar (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<InstituicaoDeEnsino> listar () throws Erro, NoSuchFieldException, SecurityException {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, NumberFormatException, NoSuchFieldException, SecurityException {
		if (ObjectUtils.isNotEmpty(id)) {
			InstituicaoDeEnsino o = getFinder().pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}
}
