package br.com.massuda.alexander.usuario.api.controller.usuario;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.utils.Imagem;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.bo.BOUsuarioImpl;
import br.com.massuda.alexander.usuario.orm.modelo.Foto;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.waiso.framework.exceptions.ErroUsuario;
import br.com.waiso.framework.utilitario.CollectionUtils;
import br.com.waiso.framework.utilitario.StringUtils;

@CrossOrigin
@Controller
@RequestMapping("/usuario")
public class UsuarioController extends Controlador<Usuario> {
	
	@Autowired
	private BOUsuarioImpl bo;
	@Autowired
	private Imagem imagem;
	@Value("${server.context-path}")
	private String api;

	@PostMapping
	public void incluir(@RequestBody Usuario usuario) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		tratarFotosInformadas(usuario);
		bo.incluir(usuario);
	}

	@PutMapping
	@ResponseBody
	public void alterar(@RequestBody Usuario usuario) throws Erro, SysErr, ErroUsuario {
		tratarFotosInformadas(usuario);
		bo.alterar(usuario);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Usuario pesquisar (@PathVariable Long id) {
		Usuario usuario = bo.pesquisar(id);
		return usuario;
	}

	@ResponseBody
	@GetMapping("/nome/{nome}")
	public List<Usuario> pesquisarPorNome (@PathVariable String nome) throws Erro, br.com.massuda.alexander.spring.framework.infra.excecoes.Erro {
		List<Usuario> usuarios = bo.pesquisarPorNome(nome);
		recuperarPossiveisImagens(usuarios);
		return usuarios;
	}

	@ResponseBody
	@GetMapping("/login/{login}")
	public Usuario pesquisar (@PathVariable String login) {
		Usuario usuario = bo.pesquisarPorLogin(login);
		recuperarPossiveisImagens(usuario);
		return usuario;
	}
	
	@ResponseBody
	@GetMapping
	public List<Usuario> listar () throws Erro {
		List<Usuario> usuarios = bo.listarUsuarios();
		recuperarPossiveisImagens(usuarios);
		return usuarios;
	}

	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Usuario usuario = bo.pesquisar(Long.parseLong(id.toString()));
			bo.excluir(usuario);
		}
	}

	
	private void tratarFotosInformadas(Usuario usuario) {
		if (existe(usuario.getFotos()) && !usuario.getFotos().isEmpty()) {
			api = api.replace("/", "");
			for (int indiceFoto = 0; indiceFoto < usuario.getFotos().size(); indiceFoto++) {
				Calendar data = Calendar.getInstance();
				String timestamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(data.getTime());
				String local = "c:/base64/{api}/{login}/{timestamp}.jpg"
						.replace("{api}", api)
						.replace("{login}", usuario.getLogin())
						.replace("{timestamp}", timestamp);
				String nomeFTP = "/public_ftp/{api}/images/{login}/{timestamp}.jpg"
						.replace("{api}", api)
						.replace("{login}", usuario.getLogin())
						.replace("{timestamp}", timestamp);
				imagem.converterParaArquivo(usuario.getFotos().get(indiceFoto).getBase64(), local, nomeFTP);
				usuario.getFotos().get(indiceFoto).setCaminho(nomeFTP);
				usuario.getFotos().get(indiceFoto).setData(data);
			}
//			usuario.getFotos().forEach(foto -> {
//				Calendar data = Calendar.getInstance();
//				String timestamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(data.getTime());
//				String local = "c:/base64/{api}/{login}/{timestamp}.jpg"
//						.replace("{api}", api)
//						.replace("{login}", usuario.getLogin())
//						.replace("{timestamp}", timestamp);
//				String nomeFTP = "/public_ftp/{api}/images/{login}/{timestamp}.jpg"
//						.replace("{api}", api)
//						.replace("{login}", usuario.getLogin())
//						.replace("{timestamp}", timestamp);
//				imagem.converterParaArquivo(foto.getBase64(), local, nomeFTP);
//				foto.setCaminho(nomeFTP);
//				foto.setData(data);
//			});
		}
	}
	
	private void recuperarPossiveisImagens(List<Usuario> usuarios) {
		if (!CollectionUtils.isBlank(usuarios)) {
			for (int indiceUsuario = 0; indiceUsuario < usuarios.size(); indiceUsuario++) {
				recuperarPossiveisImagens(usuarios.get(indiceUsuario));
			}
		}
		
	}
	
	private void recuperarPossiveisImagens(Usuario usuario) {
		if (existe(usuario.getFotos()) && !usuario.getFotos().isEmpty()) {
			for (int indiceFoto = 0; indiceFoto < usuario.getFotos().size(); indiceFoto++) {
				String local = "c:/base64/{api}/{login}/{timestamp}.jpg"
						.replace("{api}", api)
						.replace("{login}", usuario.getLogin())
						.replace("{timestamp}.jpg", usuario.getFotos().get(indiceFoto).getCaminho().split("////")[usuario.getFotos().get(indiceFoto).getCaminho().split("////").length-1]);
				String base64 = imagem.get(local, usuario.getFotos().get(indiceFoto).getCaminho());
				usuario.getFotos().get(indiceFoto).setBase64(base64);
				usuario.getFotos().get(indiceFoto).setCaminho(org.apache.commons.lang3.StringUtils.EMPTY);
			}
		}
	}
}

/*if (existe(usuario.getFotos()) && !usuario.getFotos().isEmpty()) {
try {
	String raiz = System.getProperty("catalina.home");
	File pastaArquivosTemporarios = new File(raiz + "/temp/");
	if (!pastaArquivosTemporarios.exists()) {
		pastaArquivosTemporarios.mkdirs();
	}
	try{
		String storage = "/storage/";
		File caminhoArquivo = new File(raiz+storage);
		if (!caminhoArquivo.exists()) {
			caminhoArquivo.mkdirs();
		}
		caminhoArquivo = caminhoArquivo + "/";

		while (i.hasNext()) {
			FileItem item = i.next();
			if (!item.isFormField()) {
				String nomeCampo = item.getFieldName();
				String nomeArquivo = item.getName();
				String tipoConteudo = item.getContentType();
				boolean estaEmMemoria = item.isInMemory();
				long tamanhoEmBytes = item.getSize();

				if (nomeArquivo.lastIndexOf("/") >= 0) {
					arquivo = new File(caminhoArquivo + nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")));
				} else {
					arquivo = new File(caminhoArquivo + nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")+1));
				}

				item.write(arquivo);
				Foto foto = new Foto();
				foto.setCaminho(arquivo.getAbsolutePath());
				foto.setData(GregorianCalendar.getInstance());
				foto.setDescricao(arquivo.getName());
				fotos.add(foto);
			} else {
			}
		}
	} catch (Exception e) {
		getWriter(null, resposta).println("Erro: "+e.getMessage());
	}
} catch (IOException e) {
	e.printStackTrace();
}
}*/

