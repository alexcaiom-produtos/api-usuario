package br.com.massuda.alexander.usuario.api.controller.academico;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.api.controller.Controlador;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.AreaDeEstudo;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@Controller
@RequestMapping("/academico/area-estudo")
public class AreaEstudoController extends Controlador<AreaDeEstudo> {
	
	public AreaEstudoController() {
		super(AreaDeEstudo.class);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody AreaDeEstudo o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		getDao().incluir(o);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody AreaDeEstudo o) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public AreaDeEstudo pesquisar (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<AreaDeEstudo> listar () throws Erro, NoSuchFieldException, SecurityException {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, NumberFormatException, NoSuchFieldException, SecurityException {
		if (ObjectUtils.isNotEmpty(id)) {
			AreaDeEstudo o = getFinder().pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}
}
