package br.com.massuda.alexander.usuario.api.controller.usuario;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Circulo;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/circulo")
public class CirculoController extends Controlador<Circulo> {
	
	private DAO<Circulo> dao;
	private Finder<Circulo> finder;

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody Circulo o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		getDao().incluir(o);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody Circulo o) throws Erro, SysErr, SQLException {
		getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Circulo pesquisar (@PathVariable Long id) {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Circulo> listar () throws Erro {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Circulo o = finder.pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}
	
	private DAOGenericoJDBCImpl<Circulo> getDao() {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<>(Circulo.class);
		}
		return dao;
	}
	
	private FinderJDBC<Circulo> getFinder() {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<>(Circulo.class);
		}
		return finder;
	}

}
