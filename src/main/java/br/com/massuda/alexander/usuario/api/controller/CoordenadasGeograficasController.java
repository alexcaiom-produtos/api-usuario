package br.com.massuda.alexander.usuario.api.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.CoordenadasGeograficas;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/coordenadas-geograficas")
public class CoordenadasGeograficasController extends Controlador<CoordenadasGeograficas> {
	
	public CoordenadasGeograficasController() {
		super(CoordenadasGeograficas.class);
	}

	@PostMapping
	public void incluir(@RequestBody CoordenadasGeograficas segmento) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		getDao().incluir(segmento);
	}
	
	
	@PutMapping
	@ResponseBody
	public void alterar(@RequestBody CoordenadasGeograficas segmento) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		getDao().editar(segmento);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public CoordenadasGeograficas pesquisar (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<CoordenadasGeograficas> listar () throws Erro, NoSuchFieldException, SecurityException {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, ErroUsuario, NoSuchFieldException, SecurityException {
		CoordenadasGeograficas segmento = finder.pesquisar(Long.parseLong(id.toString()));
		getDao().excluir(segmento);
	}

}
