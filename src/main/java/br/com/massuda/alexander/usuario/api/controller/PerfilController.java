package br.com.massuda.alexander.usuario.api.controller;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.orm.modelo.Perfil;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/perfil")
public class PerfilController extends Controlador<Perfil> {
	
	private DAO<Perfil> dao;
	private Finder<Perfil> finder;

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody Perfil segmento) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		getDao().incluir(segmento);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody Perfil segmento) throws Erro, SysErr, SQLException {
		getDao().editar(segmento);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Perfil pesquisar (@PathVariable Long id) {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Perfil> listar () throws Erro {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Perfil segmento = finder.pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(segmento);
		}
	}
	
	private DAOGenericoJDBCImpl<Perfil> getDao() {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<>(Perfil.class);
		}
		return dao;
	}
	
	private FinderJDBC<Perfil> getFinder() {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<>(Perfil.class);
		}
		return finder;
	}

}
