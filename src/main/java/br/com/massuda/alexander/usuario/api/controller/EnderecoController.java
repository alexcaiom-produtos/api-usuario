package br.com.massuda.alexander.usuario.api.controller;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.finder.usuario.profissional.impl.FinderSegmentoImpl;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.dao.usuario.profissional.impl.SegmentoDAOImpl;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.CoordenadasGeograficas;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Segmento;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/endereco")
public class EnderecoController extends Controlador<Endereco> {
	
	private DAO<Endereco> dao;
	private Finder<Endereco> finder;

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody Endereco o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		getDao().incluir(o);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody Endereco o) throws Erro, SysErr, SQLException {
		getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Endereco pesquisar (@PathVariable Long id) {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Endereco> listar () throws Erro {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Endereco o = finder.pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}
	
	private DAOGenericoJDBCImpl<Endereco> getDao() {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<>(Endereco.class);
		}
		return dao;
	}
	
	private FinderJDBC<Endereco> getFinder() {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<>(Endereco.class);
		}
		return finder;
	}

}
