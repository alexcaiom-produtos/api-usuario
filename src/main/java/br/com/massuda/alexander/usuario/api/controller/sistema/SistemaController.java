package br.com.massuda.alexander.usuario.api.controller.sistema;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Optional;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Funcionalidade;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Modulo;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Sistema;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@RestController
@RequestMapping("/sistemico/sistema")
public class SistemaController extends Controlador<Sistema> {
	
	private DAO<Sistema> dao;
	private Finder<Sistema> finder;

	@ResponseBody
	@PostMapping
	public Sistema incluir(@RequestBody Sistema o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		return getDao().incluir(o);
	}
	
	
	@ResponseBody
	@PutMapping
	public Sistema alterar(@RequestBody Sistema o) throws Erro, SysErr, SQLException {
		return getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Sistema pesquisarId (@PathVariable Long id) {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Sistema> pesquisar (String nome) throws Erro {
		List<Filtro> filtros = new ArrayList<>();
		if (Optional.fromNullable(nome).isPresent()) {
			filtros.add(new Filtro(String.class, "nome", TipoFiltro.IGUAL_A, nome, true));
		}
		return getFinder().pesquisar(filtros);
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Sistema o = finder.pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}

	@ResponseBody
	@GetMapping("/id/{id}/modulos")
	public Sistema pesquisarModulos (@PathVariable Long id) {
		Sistema o = getFinder().pesquisar(id);
		List<Filtro> filtros = new ArrayList<>();
		filtros.add(new Filtro(Long.class, "sistema_id", TipoFiltro.IGUAL_A, id, false));
		List<Modulo> lista = new Finder(Modulo.class).pesquisar(filtros);
		o.setModulos(lista);
		return o;
	}
	
	private DAOGenericoJDBCImpl<Sistema> getDao() {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<>(Sistema.class);
		}
		return dao;
	}
	
	private FinderJDBC<Sistema> getFinder() {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<>(Sistema.class);
		}
		return finder;
	}

}
