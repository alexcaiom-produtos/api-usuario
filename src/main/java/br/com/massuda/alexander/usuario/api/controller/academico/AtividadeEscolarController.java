package br.com.massuda.alexander.usuario.api.controller.academico;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.api.controller.Controlador;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.AtividadeEscolar;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/academico/atividade-escolar")
public class AtividadeEscolarController extends Controlador<AtividadeEscolar> {
	
	public AtividadeEscolarController() {
		super(AtividadeEscolar.class);
		// TODO Auto-generated constructor stub
	}


	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public void incluir(@RequestBody AtividadeEscolar o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		getDao().incluir(o);
	}
	
	
	@PutMapping
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public void alterar(@RequestBody AtividadeEscolar o) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public AtividadeEscolar pesquisar (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<AtividadeEscolar> listar () throws Erro, NoSuchFieldException, SecurityException {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, ErroUsuario, NoSuchFieldException, SecurityException {
		if (ObjectUtils.isNotEmpty(id)) {
			AtividadeEscolar o = getFinder().pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}
//	
//	private DAOGenericoJDBCImpl<AtividadeEscolar> getDao() {
//		if (ObjectUtils.isEmpty(dao)) {
//			dao = new DAO<>(AtividadeEscolar.class);
//		}
//		return dao;
//	}
//	
//	private FinderJDBC<AtividadeEscolar> getFinder() {
//		if (ObjectUtils.isEmpty(finder)) {
//			finder = new Finder<>(AtividadeEscolar.class);
//		}
//		return finder;
//	}

}
