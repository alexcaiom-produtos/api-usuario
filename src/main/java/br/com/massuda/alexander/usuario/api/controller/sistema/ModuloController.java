package br.com.massuda.alexander.usuario.api.controller.sistema;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.api.controller.Controlador;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Funcionalidade;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Modulo;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@Controller
@RequestMapping("/sistemico/modulo")
public class ModuloController extends Controlador<Modulo> {
	
	public ModuloController() {
		super(Modulo.class);
	}

	@PostMapping
	@ResponseBody
	public Modulo incluir(@RequestBody Modulo o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		return getDao().incluir(o);
	}
	
	
	@PutMapping
	@ResponseBody
	public Modulo alterar(@RequestBody Modulo o) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		return getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Modulo pesquisar (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Modulo> listar () throws Erro, NoSuchFieldException, SecurityException {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, ErroUsuario, NoSuchFieldException, SecurityException {
		if (ObjectUtils.isNotEmpty(id)) {
			Modulo o = finder.pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}

	@ResponseBody
	@GetMapping("/id/{id}/funcionalidades")
	public Modulo pesquisarFuncionalidades (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		Modulo modulo = getFinder().pesquisar(id);
		List<Filtro> filtros = new ArrayList<>();
		filtros.add(new Filtro(Long.class, "modulo_id", TipoFiltro.IGUAL_A, id, false));
		List<Funcionalidade> funcionalidades = new Finder(Funcionalidade.class).pesquisar(filtros);
		modulo.setFuncionalidades(funcionalidades);
		return modulo;
	}
	
}
