package br.com.massuda.alexander.usuario.api.controller.profissional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.controller.Controlador;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.api.mapper.Mapper;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.finder.usuario.profissional.IFinderEmpresa;
import br.com.massuda.alexander.usuario.dao.finder.usuario.profissional.impl.FinderEmpresaImpl;
import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.dao.usuario.profissional.IEmpresaDAO;
import br.com.massuda.alexander.usuario.dao.usuario.profissional.impl.EmpresaDAOImpl;
import br.com.massuda.alexander.usuario.orm.bo.BOUsuarioImpl;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;
import br.com.waiso.framework.exceptions.ErroUsuario;
import br.com.waiso.framework.utilitario.CollectionUtils;

@CrossOrigin
@Controller
@RequestMapping("/empresa")
public class EmpresaController extends Controlador<Empresa> {
	
	private DAO<Empresa> dao;
	private FinderJDBC<Empresa> finder;
	@Autowired
	private BOUsuarioImpl boUsuario;

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@RequestBody Empresa empresa) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException {
		if (CollectionUtils.isBlank(empresa.getUsuarios())) {
			Usuario u = Mapper.from(empresa);
			Usuario usuario = boUsuario.incluir(u);
			List<Usuario> usuarios = new ArrayList<>();
			usuarios.add(usuario);
			empresa.setUsuarios(usuarios);
		}
		getDao().incluir(empresa);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody Empresa empresa) throws Erro, SysErr, SQLException, ErroNegocio, ErroUsuario {
		if (CollectionUtils.isBlank(empresa.getUsuarios())) {
			Usuario u = Mapper.from(empresa);
			Usuario usuario = boUsuario.incluir(u);
			List<Usuario> usuarios = new ArrayList<>();
			usuarios.add(usuario);
			empresa.setUsuarios(usuarios);
		}
		getDao().editar(empresa);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Empresa pesquisar (@PathVariable Long id) {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Empresa> listar () throws Erro {
		return getFinder().listar();
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr {
		if (ObjectUtils.isNotEmpty(id)) {
			Empresa empresa = getFinder().pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(empresa);
		}
	}

	private DAO<Empresa> getDao() {
		if (ObjectUtils.isEmpty(dao)) {
			dao = new DAO<>(Empresa.class);
		}
		return dao;
	}
	
	private FinderJDBC<Empresa> getFinder() {
		if (ObjectUtils.isEmpty(finder)) {
			finder = new Finder<>(Empresa.class);
		}
		return finder;
	}
}
