package br.com.massuda.alexander.usuario.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.com.massuda.alexander.usuario.orm.bo.config.Configuracao;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@Import({	
	Configuracao.class,
	br.com.massuda.alexander.spring.framework.infra.config.Configuracao.class,
	br.com.massuda.alexander.spring.framework.infra.web.config.Configuracao.class
	})
@Configuration
public class ApiUsuarioApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ApiUsuarioApplication.class, args);
	}
	
}
