package br.com.massuda.alexander.usuario.api.controller.sistema.autorizacao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Optional;

import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.excecoes.SysErr;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.autorizacao.Permissao;
import br.com.waiso.framework.exceptions.ErroUsuario;

@CrossOrigin
@RestController
@RequestMapping("/sistemico/permissao")
public class PermissaoController extends br.com.massuda.alexander.usuario.api.controller.Controlador<Permissao> {
	
	public PermissaoController() {
		super(Permissao.class);
	}


	@ResponseBody
	@PostMapping
	public Permissao incluir(@RequestBody Permissao o) throws Erro, SysErr, ErroNegocio, ErroUsuario, SQLException, NoSuchFieldException, SecurityException {
		return getDao().incluir(o);
	}
	
	
	@ResponseBody
	@PutMapping
	public Permissao alterar(@RequestBody Permissao o) throws Erro, SysErr, SQLException, NoSuchFieldException, SecurityException {
		return getDao().editar(o);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Permissao pesquisarId (@PathVariable Long id) throws NoSuchFieldException, SecurityException {
		return getFinder().pesquisar(id);
	}

	@ResponseBody
	@GetMapping
	public List<Permissao> pesquisar (String nome) throws Erro, NoSuchFieldException, SecurityException {
		List<Filtro> filtros = new ArrayList<>();
		if (Optional.fromNullable(nome).isPresent()) {
			filtros.add(new Filtro(String.class, "nome", TipoFiltro.IGUAL_A, nome, true));
		}
		return getFinder().pesquisar(filtros);
	}
	
	@ResponseBody
	@DeleteMapping("/id/{id}")
	public void excluir (@PathVariable("id") Integer id) throws Erro, SysErr, NumberFormatException, NoSuchFieldException, SecurityException {
		if (ObjectUtils.isNotEmpty(id)) {
			Permissao o = getFinder().pesquisar(Long.parseLong(id.toString()));
			getDao().excluir(o);
		}
	}

	@ResponseBody
	@GetMapping("/usuario/id/{id}/permissoes")
	public List<Permissao> pesquisarUsuarios (@PathVariable Long id) {
		List<Filtro> filtros = new ArrayList<>();
		filtros.add(new Filtro(Long.class, "usuario_id", TipoFiltro.IGUAL_A, id, false));
		return new Finder(Permissao.class).pesquisar(filtros);
	}
	
}
