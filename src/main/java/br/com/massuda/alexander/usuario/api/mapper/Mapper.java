/**
 * 
 */
package br.com.massuda.alexander.usuario.api.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.Escolaridade;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.EscolaridadeDto;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;
import br.com.waiso.framework.utilitario.DateUtils;

/**
 * @author Alex
 *
 */
public class Mapper {

	public static Usuario from(Empresa empresa) {
		Usuario u = new Usuario();
		u.setEmail(empresa.getEmail());
		u.setDataDeNascimento(empresa.getDataDeNascimento());
		u.setIdentificacao(empresa.getIdentificacao());
		u.setNome(empresa.getNome());
		u.setTipo(empresa.getTipo());
		return u;
	}

	public static Escolaridade to(EscolaridadeDto dto) {
		Escolaridade o = new Escolaridade();
		BeanUtils.copyProperties(dto, o);
		o.setInicio(DateUtils.getInstance().convert(dto.getInicio()));
		o.setFim(DateUtils.getInstance().convert(dto.getFim()));
		return o;
	}

	public static EscolaridadeDto from(Escolaridade o) {
		EscolaridadeDto dto = new EscolaridadeDto();
		BeanUtils.copyProperties(o, dto);
		dto.setInicio(DateUtils.getInstance().dateToString(o.getInicio(), DateUtils.BRAZILIAN_PATTERN));
		dto.setFim(DateUtils.getInstance().dateToString(o.getFim(), DateUtils.BRAZILIAN_PATTERN));
		return dto;
	}

	public static List<EscolaridadeDto> from(List<Escolaridade> list) {
		List<EscolaridadeDto> dtos = new ArrayList<>();
		list.forEach(o -> {
			dtos.add(from(o));
		});
		return dtos;
	}

}
